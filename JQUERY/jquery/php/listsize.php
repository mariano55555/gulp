<?php
$pageSize = 5;
if (isset($_REQUEST["pageSize"])) {
  $pageSize = $_REQUEST["pageSize"];
}
?>
<table border="1">
  <thead>
    <tr>
        <td>#</td>
        <td>Nombre</td>
        <td>Apellido</td>
    </tr>
  </thead>
  <tbody>
    <?php for ($i=0; $i < $pageSize; $i++) { ?>
      <tr>
        <td><?php echo $i + 1; ?></td>
        <td>Juan<?php echo $i + 1; ?></td>
        <td>Flores <?php echo $i + 1; ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>
