<?php
$pagesize = isset($_POST["PageSize"]) ? $_POST["PageSize"] : 5;
?>
<table border="1">
  <thead>
    <tr>
        <td>#</td>
        <td>Nombre</td>
        <td>Apellido</td>
    </tr>
  </thead>
  <tbody>
    <?php for ($i=0; $i < $pagesize; $i++) { ?>
      <tr>
        <td><?php echo $i + 1; ?></td>
        <td>Juan<?php echo $i + 1; ?></td>
        <td>Flores <?php echo $i + 1; ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>
