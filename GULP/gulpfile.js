
//IMPORTAR PLUGINS
var
  gulp       = require("gulp"),
  imagemin   = require("gulp-imagemin"),
  newer      = require("gulp-newer"),
  htmlclean  = require("gulp-htmlclean"),
  size       = require("gulp-size"),
  del        = require("del"),
  imacss     = require("gulp-imacss"),
  //concat     = require("gulp-concat"),
  //deporder   = require("gulp-deporder"),
  pleeease = require("gulp-pleeease"),
  //stripdebug = require('gulp-strip-debug'),
  //uglify = require('gulp-uglify'),
  sass        = require("gulp-sass"),
  jshint      = require("gulp-jshint"),
  preprocess  = require("gulp-preprocess"),
  browsersync = require("browser-sync"),
  pkg         = require('./package.json');




// DEFINIR VARIABLES DE TRABAJO
var
  devBuild = ((process.env.NODE_ENV || 'development').trim().toLowerCase() !== 'production'),
  source = "source/",
  dest   = "build/",
  images = {
    in : source + 'images/*.*', // source/images/*.*
    out: dest + 'images/'       // build/images/
  },
  html= {
    in   : source + '*.html',
    watch: [source + '*.html', source + 'template/**/*'],
    out  : dest,
    context: {
      devBuild: devBuild,
      author  : pkg.author,
      version : pkg.version
    }
  },
  imguri={
    in       : source + 'images/inline/*',
    out      : source + 'scss/images/',
    filename : "_datauri.scss",
    namespace: "img"
  },
  css = {
      in   : source + 'scss/main.scss',
      //watch: [source + 'scss/**/*'],
      watch: [source + 'scss/**/*', "!" + imguri.out + imguri.filename],
      out  : dest+ 'css/',
      sassOpts : {
        outputStyle: 'nested',
        imagePath: '../images',
        precision : 3,
        errLogToConsole: true
      },
      pleeeaseOpts: {
        autoprefixer: {browsers: ['last 2 versions', '> 2%']},
        rem: ['16px'],
        pseudoElements: true,
        mpacker: true,
        minifier: !devBuild
      }
  },
  fonts={
    in   : source + 'fonts/*.*',
    out  : css.out+ 'fonts/',
  },

  js={
    in      : source + 'js/**/*',
    out     : dest + 'js/',
    filename: 'main.js'
  },
  syncOpts = {
    server: {
      baseDir: dest,
      index: 'index.html'
    },
    open: true,
    notify: true
  };



//TAREAS

//limpiar
gulp.task("clean", function(){
  del([
      dest + '*'
  ]);
});

//images
gulp.task("images", function(){
    return gulp.src(images.in)
          .pipe(newer(images.out))
          .pipe(imagemin())
          .pipe(gulp.dest(images.out));
});


//console.log(pkg.name + ' '+pkg.version + ' ' + pkg.author + ' Ambiente: ' + (devBuild ? 'development' : 'production'));

//html
gulp.task("html", function() {
    var pagina = gulp.src(html.in).pipe(preprocess({context: html.context}));
    if (!devBuild) {
      pagina = pagina
            .pipe(size({title: "HTML inicio: " }))
            .pipe(htmlclean())
            .pipe(size({title: "HTML fin: " }));
    }
    return pagina.pipe(gulp.dest(html.out));
});



// Traea para procesar js
/*
tarea js v1.0
gulp.task('js', function(){
  return gulp.src(js.in)
      .pipe(newer(js.out))
      .pipe(jshint())
      .pipe(jshint.reporter('default'))
      .pipe(jshint.reporter('fail'))
      .pipe(gulp.dest(js.out));
});*/


// tarea js v2.0
// gulp.task('js', function(){
//   if (debBuild) {
//     return gulp.src(js.in)
//         .pipe(newer(js.out))
//         .pipe(jshint())
//         .pipe(jshint.reporter('default'))
//         .pipe(jshint.reporter('fail'))
//         .pipe(gulp.dest(js.out));
//   }else{
//     del([
//       dest + 'js/*'
//     ]);
//
//     return gulp.src(js.in)
//         .pipe(deporder())
//         .pipe(concat(js.filename))
//         .pipe(gulp.dest(js.out));
//   }
//
// });


// tarea js v3.0
// gulp.task('js', function(){
//   if (debBuild) {
//     return gulp.src(js.in)
//         .pipe(newer(js.out))
//         .pipe(jshint())
//         .pipe(jshint.reporter('default'))
//         .pipe(jshint.reporter('fail'))
//         .pipe(gulp.dest(js.out));
//   }else{
//     del([
//       dest + 'js/*'
//     ]);
//
//     return gulp.src(js.in)
//         .pipe(deporder())
//         .pipe(concat(js.filename))
//         .pipe(size({title: 'JS inicial: '}))
//         .pipe(stripdebug())
//         .pipe(uglify())
//         .pipe(size({title: 'JS salida: '}))
//         .pipe(gulp.dest(js.out));
//   }
//
// });


// tarea para fuentes
gulp.task("fonts", function(){
  return gulp.src(fonts.in)
      .pipe(newer(fonts.out))
      .pipe(gulp.dest(fonts.out));
});

// tarea para datauri
gulp.task("imguri", function(){
  return gulp.src(imguri.in)
      .pipe(imagemin())
      .pipe(imacss(imguri.filename, imguri.namespace))
      .pipe(gulp.dest(imguri.out));
});

//broser sync
gulp.task("browsersync", function(){
    browsersync(syncOpts);
});


// tarea para css
gulp.task("sass", ['imguri'], function(){
  return gulp.src(css.in)
       .pipe(sass(css.sassOpts))
       .pipe(size({title: 'CSS entrada: '}))
       .pipe(pleeease(css.pleeeaseOpts))
       .pipe(size({title: 'CSS salida: '}))
       .pipe(gulp.dest(css.out));
      //.pipe(browsersync.reload({stream: true}));
});

//crear tarea default
gulp.task("default", ['images', 'html', 'fonts', 'sass' /*, 'js', 'browsersync'*/],function(){
    //LOGICA DE NEGOCIOS
    gulp.watch(images.in, ['images']);

    gulp.watch(html.watch, ['html']);
    //gulp.watch(html.watch, ['html', browsersync.reload]);

    //gulp.watch(css.watch, ['sass']);
    gulp.watch([css.watch, imguri.in], ['sass']);


    //gulp.watch(js.in, ['js']);
    //gulp.watch(js.in, ['js', browsersync.reload]);

});
