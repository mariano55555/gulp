'urlManager' => [
      'class' => 'yii\web\UrlManager',
      // Disable index.php
      'showScriptName' => false,
      // Disable r= routes
      'enablePrettyUrl' => true,
      'rules' => array(
            '<controller:[\w\-]+>/<id:\d+>' => '<controller>/view',
            '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
            '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
            '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>/<anios:\d+>' => '<controller>/<action>',
            'site/captcha/<refresh:\d+>' => 'site/captcha',
            'site/captcha/<v:\w+>' => 'site/captcha',
            '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
            '<module:[\w\-]+>/<controller:[\w\-]+>' => '<module>/<controller>/index'
      ),
],
