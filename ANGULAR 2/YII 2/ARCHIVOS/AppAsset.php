<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
    ];
    public $js = [
      'js/lib/angular2/es6-shim.min.js',
      'js/lib/angular2/system-polyfills.js',
      'js/lib/angular2/angular2-polyfills.js',
      'js/lib/angular2/system.src.js',
      'js/lib/angular2/Rx.js',
      'js/lib/angular2/angular2.dev.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
      //  'yii\bootstrap\BootstrapAsset',
    ];
}
