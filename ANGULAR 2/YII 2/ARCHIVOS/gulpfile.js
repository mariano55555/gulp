//IMPORTAR PLUGINS
var
  gulp       = require("gulp"),
  del        = require("del"),
  webserver  = require('gulp-webserver'),
  typescript = require('gulp-typescript'),
  sourcemaps = require('gulp-sourcemaps'),
  tscConfig  = require('./tsconfig.json');


var appSrc = 'frontend/web/',
      tsSrc = 'process/typescript/';

var
   assetsFrontEnd = 'frontend/assets/',
   frontEnd = 'frontend/web/',
   html= {
     watch: [assetsFrontEnd + '*.php']
   };

   gulp.task('clean', function(){
     del([
       frontEnd + 'assets/*',
       '!.gitignore'
     ]);
   });


   gulp.task('html', function() {
     gulp.src(appSrc + '**/*.html');
   });

   gulp.task('css', function() {
     gulp.src(appSrc + '**/*.css');
   });

   gulp.task('copylibs', function() {
     return gulp
       .src([
          'node_modules/es6-shim/es6-shim.min.js',
          'node_modules/systemjs/dist/system-polyfills.js',
          'node_modules/angular2/bundles/angular2-polyfills.js',
          'node_modules/systemjs/dist/system.src.js',
          'node_modules/rxjs/bundles/Rx.js',
          'node_modules/angular2/bundles/angular2.dev.js'
       ])
       .pipe(gulp.dest(appSrc + 'js/lib/angular2'));
   });



   gulp.task('typescript', function () {
     return gulp
       .src(tsSrc + '**/*.ts')
       .pipe(sourcemaps.init())
       .pipe(typescript(tscConfig.compilerOptions))
       .pipe(sourcemaps.write('.'))
       .pipe(gulp.dest(appSrc + 'js/'));
   });

// TAREA DEFAULT
gulp.task('default', ['copylibs', 'typescript'], function(){
  gulp.watch(html.watch, ['clean']);
  gulp.watch(tsSrc + '**/*.ts', ['typescript']);
  gulp.watch(appSrc + 'css/*.css', ['css']);
  gulp.watch(appSrc + '**/*.html', ['html']);

});
